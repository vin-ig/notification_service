## Сервис по доставке сообщений
В данном проекте реализуем MVP сервиса по доставке SMS-сообщение клиентам.
___
### Стек:
- Django
- Postgres
- Redis
- Celery
- Flower
___
### Реализованы методы:
#### Клиенты:
`CRUD /notifications/client/`
#### Рассылки:
`POST /notifications/mailing/create/`

`GET /notifications/mailing/` (с группировкой по отправленным сообщениям)

`GET PUT PATCH DELETE /notifications/mailing/<id>/`
___
### Запуск проекта
Для установки проекта необходимо клонировать репозиторий на локальную машину, установить зависимости
и активировать виртуальное окружение.
```
git clone https://gitlab.com/vin-ig/notification_service.git
python -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```
В качестве переменных окружения можно
использовать данные из файла `.env.examle` (достаточно переименовать файл в `.env`).

Далее необходимо:

`docker-compose up -d --build`  _# собрать и поднять контейнеры в докере_

`python manage.py makemigrations`  _# создать миграции_

`python manage.py migrate`  _# накатить миграции_

`python manage.py runserver`  _# запустить django-сервер_

`celery -A FR worker -l info`  _# запустить celery_

`celery -A FR flower --port=5555`  _# запустить flower_

___
