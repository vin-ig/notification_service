from rest_framework import serializers

from notification_service.models import Client, Mailing, Message


class ClientSerializer(serializers.ModelSerializer):
	class Meta:
		model = Client
		fields = "__all__"


class MailingListSerializer(serializers.ModelSerializer):
	sending_messages = serializers.IntegerField()

	class Meta:
		model = Mailing
		fields = "__all__"


class MailingSerializer(serializers.ModelSerializer):
	class Meta:
		model = Mailing
		fields = "__all__"


class MessageListSerializer(serializers.ModelSerializer):
	class Meta:
		model = Message
		fields = "__all__"
