from django.urls import path
from rest_framework import routers

from notification_service import views

router = routers.SimpleRouter()
router.register('client', views.ClientViewSet)

urlpatterns = [
    path("mailing/create/", views.MailingCreateView.as_view(), name="mailing create"),
    path("mailing/", views.MailingListView.as_view(), name="mailing list"),
    path("mailing/<int:pk>/", views.MailingView.as_view(), name="mailing retrieve/update/delete"),
]

urlpatterns += router.urls
