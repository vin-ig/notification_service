from django.db.models.signals import post_save
from django.dispatch import receiver
from django.db.models import Q

from .models import Mailing, Client, Message
from .tasks import send_message


@receiver(post_save, sender=Mailing, dispatch_uid="create_message")
def create_message(sender, instance, **kwargs):
	mailing = Mailing.objects.filter(id=instance.id).first()
	clients = Client.objects.filter(Q(phone_code=mailing.filter) | Q(tag__icontains=mailing.filter))

	for client in clients:
		Message.objects.create(
			status=Message.TO_SEND,
			client_id=client.id,
			mailing_id=instance.id
		)
		message = Message.objects.filter(mailing_id=instance.id, client_id=client.id).first()
		data = {
			'id': message.id,
			"phone": client.phone,
			"text": mailing.text
		}
		client_id = client.id
		mailing_id = mailing.id

		send_message.apply_async((data, client_id, mailing_id), expires=mailing.end_time)
