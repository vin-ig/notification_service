from django.db import models


class Mailing(models.Model):
	class Meta:
		verbose_name = 'Рассылка'
		verbose_name_plural = 'Рассылки'

	start_time = models.DateTimeField(verbose_name='Дата рассылки')
	text = models.CharField(max_length=500, verbose_name='Текст рассылки')
	filter = models.CharField(max_length=50, verbose_name='Фильтр свойств клиентов')
	end_time = models.DateTimeField(verbose_name='Дата окончания рассылки')


class Client(models.Model):
	class Meta:
		verbose_name = 'Клиент'
		verbose_name_plural = 'Клиенты'

	phone = models.CharField(max_length=11, verbose_name='Телефон')
	phone_code = models.CharField(max_length=3, verbose_name='Код мобильного оператора')
	tag = models.CharField(max_length=50, verbose_name='Тэг')
	timezone = models.IntegerField(verbose_name='Часовой пояс')


class Message(models.Model):
	TO_SEND, WAITING, SENT, FAILED = 'to_send', 'waiting', 'sent', 'failed'
	STATUS = [
		(TO_SEND, 'К отправке'),
		(WAITING, 'Ожидание отправки'),
		(SENT, 'Отправлено'),
		(FAILED, 'Не отправлено'),
	]

	class Meta:
		verbose_name = 'Сообщение'
		verbose_name_plural = 'Сообщения'

	send_time = models.DateTimeField(verbose_name='Дата отправки', blank=True, null=True)
	status = models.CharField(max_length=10, choices=STATUS, default=WAITING, verbose_name='Статус отправки')
	mailing = models.ForeignKey(Mailing, on_delete=models.CASCADE)
	client = models.ForeignKey(Client, on_delete=models.CASCADE)
