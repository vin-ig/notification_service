from django.db.models import Count
from rest_framework.generics import CreateAPIView, RetrieveUpdateDestroyAPIView, ListAPIView
from rest_framework.viewsets import ModelViewSet

from notification_service import serializers
from notification_service.models import Client, Mailing, Message


class ClientViewSet(ModelViewSet):
	queryset = Client.objects.all()
	serializer_class = serializers.ClientSerializer


class MailingCreateView(CreateAPIView):
	model = Mailing
	serializer_class = serializers.MailingSerializer


class MailingListView(ListAPIView):
	queryset = Mailing.objects.filter(message__status=Message.TO_SEND).\
		annotate(sending_messages=Count('message__status'))
	serializer_class = serializers.MailingListSerializer


class MailingView(RetrieveUpdateDestroyAPIView):
	queryset = Mailing.objects.all()
	serializer_class = serializers.MailingSerializer
